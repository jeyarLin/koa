MySQL 中定义数据字段的类型对你数据库的优化是非常重要的。MySQL 支持多种类型，大致可以分为三类：数值、日期/时间和字符串(字符)类型。

# 数值类型

MySQL 支持所有标准 SQL 数值数据类型。这些类型包括严格数值数据类型(INTEGER、SMALLINT、DECIMAL 和 NUMERIC)，以及近似数值数据类型(FLOAT、REAL 和 DOUBLE PRECISION)。

关键字INT是INTEGER的同义词，关键字DEC是DECIMAL的同义词。BIT数据类型保存位字段值，并且支持 MyISAM、MEMORY、InnoDB 和 BDB表。
作为 SQL 标准的扩展，MySQL 也支持整数类型 TINYINT、MEDIUMINT 和 BIGINT。下面的表显示了需要的每个整数类型的存储和范围：

| 类型 | 大小 | 范围(有符号) | 范围(无符号) | 用途 |
| --- | --- | --- | --- | --- |
| TINYINT | 1 Bytes | (-128，127) | (0，255) | 小整数 |
| SMALLINT | 2 Bytes | (-32 768，32 767) | (0，65 535) | 大整数 |
| MEDIUMINT | 3 Bytes | (-8 388 608，8 388 607) | (0，16 777 215) | 大整数 |
| INT或INTEGER | 4 Bytes | (-2 147 483 648，2 147 483 647) | (0，4 294 967 295) | 大整数 |
| BIGINT | 8 Bytes | (-9,223,372,036,854,775,808，9 223 372 036 854 775 807) | (0，18 446 744 073 709 551 615) | 极大整数值 |
| FLOAT | 4 Bytes | (-3.402 823 466 E+38，-1.175 494 351 E-38)，0，(1.175 494 351 E-38，3.402 823 466 351 E+38) | 0，(1.175 494 351 E-38，3.402 823 466 E+38) | 单精度浮点数值 |
| DOUBLE | 8 Bytes | (-1.797 693 134 862 315 7 E+308，-2.225 073 858 507 201 4 E-308)，0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308) | 0，(2.225 073 858 507 201 4 E-308，1.797 693 134 862 315 7 E+308) | 双精度浮点数值 |
| DECIMAL | 对DECIMAL(M,D) ，如果M>D，为M+2否则为D+2 | 依赖于M和D的值 | 依赖于M和D的值 | 小数值 |

## FLOAT 与 DOUBLE 差异（单精度与双精度有什么区别）：

- 最本质的区别：单精度，也就是 float ，在 32 位机器上用 4 个字节来存储的；而双精度double是用 8 个字节来存储的，由于存储位不同，他们能表示的数值的范围就不同，也就是能准确表示的数的位数就不同
- 所占的内存不同：单精度浮点数占用4个字节（32位）存储空间来存储一个浮点数，包括符号位1位，阶码8位，尾数23位；而双精度浮点数使用 8个字节（64位）存储空间来存储一个浮点数，包括符号位1位，阶码11位，尾数52位。
- 所存的数值范围不同：单精度浮点数的数值范围为-3.4E38～3.4E38；而双精度浮点数可以表示的数字的绝对值范围大约是：-2.23E308 ~ 1.79E308。E表示10的多少次方，如3.4E38指的是3.4乘以10的38次方
- 十进制下的位数不同：单精度浮点数最多有7位十进制有效数字，如果某个数的有效数字位数超过7位，当把它定义为单精度变量时，超出的部分会自动四舍五入；双精度浮点数可以表示十进制的15或16位有效数字，超出的部分也会自动四舍五入。

# 日期和时间类型

表示时间值的日期和时间类型为DATETIME、DATE、TIMESTAMP、TIME和YEAR。每个时间类型有一个有效值范围和一个"零"值，当指定不合法的MySQL不能表示的值时使用"零"值。

| 类型 | 大小 | 范围 | 格式 | 用途 |
| --- | --- | --- | --- | --- |
| DATE | 3 Bytes | 1000-01-01/9999-12-31 | YYYY-MM-DD | 日期值 |
| TIME | 3 Bytes | '-838:59:59'/'838:59:59' | HH:MM:SS | 时间值或持续时间 |
| YEAR | 1 Bytes | 1901/2155 | YYYY | 年份值 |
| DATETIME | 8 Bytes | '1000-01-01 00:00:00' 到 '9999-12-31 23:59:59' | YYYY-MM-DD hh:mm:ss | 混合日期和时间值 |
| TIMESTAMP | 4 Bytes | '1970-01-01 00:00:01' UTC 到 '2038-01-19 03:14:07' UTC。结束时间是第 2147483647 秒，北京时间 2038-1-19 11:14:07，格林尼治时间 2038年1月19日 凌晨 03:14:07 | YYYY-MM-DD hh:mm:ss | 混合日期和时间值，时间戳 |



# 字符串类型

字符串类型指CHAR、VARCHAR、BINARY、VARBINARY、BLOB、TEXT、ENUM和SET。下面的表显示了需要的每个字符串类型的存储和范围：

| 类型 | 范围 | 大小 |
| --- | --- | --- |
| CHAR | 0-255 Bytes | 定长字符串 |
| VARCHAR | 0-65535 Bytes | 变长字符串 |
| TINYBLOB | 0-255 Bytes | 不超过 255 个字符的二进制字符串 |
| TINYTEXT | 0-255 Bytes | 短文本字符串 |
| BLOB | 0-65535 Bytes | 二进制形式的长文本数据 |
| TEXT | 0-65535 Bytes | 长文本数据 |
| MEDIUMBLOB | 0-16 777 215 Bytes | 二进制形式的中等长度文本数据 |
| MEDIUMTEXT | 0-16 777 215 Bytes | 中等长度文本数据 |
| LONGBLOB | 0-4 294 967 295 Bytes | 二进制形式的极大文本数据 |
| LONGTEXT | 0-4 294 967 295 Bytes | 极大文本数据 |


## 注意：

- CHAR(n) 和 VARCHAR(n) 中括号中 n 代表字符的个数，并不代表字节个数，比如 CHAR(30) 就可以存储 30 个字符。
- CHAR 和 VARCHAR 类型类似，但它们保存和检索的方式不同。它们的最大长度和是否尾部空格被保留等方面也不同，如果插入的数据不足CHAR的指定长度，在存储数据时，CHAR会用空格填充到指定长度。在存储或检索过程中不进行大小写转换。
- BINARY 和 VARBINARY 类似于 CHAR 和 VARCHAR，不同的是它们包含二进制字符串而不要非二进制字符串。也就是说，它们包含字节字符串而不是字符字符串。这说明它们没有字符集，并且排序和比较基于列值字节的数值值。
- BLOB 是一个二进制大对象，可以容纳可变数量的数据。有 4 种 BLOB 类型：TINYBLOB、BLOB、MEDIUMBLOB 和 LONGBLOB。它们区别在于可容纳存储范围不同。
有 4 种 TEXT 类型：TINYTEXT、TEXT、MEDIUMTEXT 和 LONGTEXT。对应的这 4 种 BLOB 类型，可存储的最大长度不同，可根据实际情况选择。

mysql> desc zd_users;
+-----------+--------------+------+-----+---------+----------------+
| Field     | Type         | Null | Key | Default | Extra          |
+-----------+--------------+------+-----+---------+----------------+
| id        | int          | NO   | PRI | NULL    | auto_increment |
| user_name | varchar(255) | NO   | UNI | NULL    |                |
| password  | char(64)     | NO   |     | NULL    |                |
| is_admin  | tinyint(1)   | NO   |     | 0       |                |
| createdAt | datetime     | NO   |     | NULL    |                |
| updatedAt | datetime     | NO   |     | NULL    |                |
+-----------+--------------+------+-----+---------+----------------+
6 rows in set (0.00 sec)

mysql> desc zd_goods;
+-------------+---------------+------+-----+---------+----------------+
| Field       | Type          | Null | Key | Default | Extra          |
+-------------+---------------+------+-----+---------+----------------+
| id          | int           | NO   | PRI | NULL    | auto_increment |
| goods_name  | varchar(255)  | NO   |     | NULL    |                |
| goods_price | decimal(10,2) | NO   |     | NULL    |                |
| goods_num   | int           | NO   |     | NULL    |                |
| goods_img   | varchar(255)  | NO   |     | NULL    |                |
| createdAt   | datetime      | NO   |     | NULL    |                |
| updatedAt   | datetime      | NO   |     | NULL    |                |
| deletedAt   | datetime      | YES  |     | NULL    |                |
+-------------+---------------+------+-----+---------+----------------+
8 rows in set (0.00 sec)

mysql> desc zd_carts;
+-----------+------------+------+-----+---------+----------------+
| Field     | Type       | Null | Key | Default | Extra          |
+-----------+------------+------+-----+---------+----------------+
| id        | int        | NO   | PRI | NULL    | auto_increment |
| goods_id  | int        | NO   |     | NULL    |                |
| user_id   | int        | NO   |     | NULL    |                |
| number    | int        | NO   |     | 1       |                |
| selected  | tinyint(1) | NO   |     | 1       |                |
| createdAt | datetime   | NO   |     | NULL    |                |
| updatedAt | datetime   | NO   |     | NULL    |                |
+-----------+------------+------+-----+---------+----------------+
7 rows in set (0.01 sec)

mysql> desc zd_addresses;
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | int          | NO   | PRI | NULL    | auto_increment |
| user_id    | int          | NO   |     | NULL    |                |
| consignee  | varchar(255) | NO   |     | NULL    |                |
| phone      | char(11)     | NO   |     | NULL    |                |
| address    | varchar(255) | NO   |     | NULL    |                |
| is_default | tinyint(1)   | NO   |     | 0       |                |
| createdAt  | datetime     | NO   |     | NULL    |                |
| updatedAt  | datetime     | NO   |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+
8 rows in set (0.00 sec)

mysql> desc zd_orders;
+--------------+---------------+------+-----+---------+----------------+
| Field        | Type          | Null | Key | Default | Extra          |
+--------------+---------------+------+-----+---------+----------------+
| id           | int           | NO   | PRI | NULL    | auto_increment |
| user_id      | int           | NO   |     | NULL    |                |
| address_id   | int           | NO   |     | NULL    |                |
| goods_info   | text          | NO   |     | NULL    |                |
| total        | decimal(10,2) | NO   |     | NULL    |                |
| order_number | char(16)      | NO   |     | NULL    |                |
| status       | tinyint       | NO   |     | 0       |                |
| createdAt    | datetime      | NO   |     | NULL    |                |
| updatedAt    | datetime      | NO   |     | NULL    |                |
+--------------+---------------+------+-----+---------+----------------+
9 rows in set (0.00 sec)

