# 21-商品图片静态显示

- koa-static版本^5.0.0

```js
npm i koa-static
```

- 在src/app/index.js中导入使用

```js
const KoaStatic = require("koa-static");

app.use(KoaStatic(path.join(__dirname, "../upload"))); // 在router之前

```

# 22-集成统一的参数格式校验

- koa-parameter版本^3.0.1

```js
npm i koa-parameter
```

- 在src/app/index.js导入

```js
const parameter = require("koa-parameter");
app.use(parameter(app));
```

- 在中间件中使用

```js
const { validator } = require("../middleware/goods.middleware");
```

# 31-购物车列表

- get /carts 
- 入参：pageNum,pageSize

```js
{
    "code": 0,
    "message": "获取购物车列表成功",
    "result": {
        "pageNum": "1",
        "pageSize": "10",
        "total": 1,
        "list": [
            {
                "id": 1,
                "number": 3,
                "selected": true,
                "goods_info": {
                    "id": 1,
                    "goods_name": "蓝牙音箱plus",
                    "goods_price": "199.00",
                    "goods_img": "a985abd874b5b8af6b676e500.png"
                }
            }
        ]
    }
}
```

# 33-删除购物车

DELETE /carts

ids: [1,2,3]

src/app/index.js koa-body 

```js
parsedMethods: ["POST", "PUT", "PATCH", "DELETE"],
```

# 34-全选与全不选

- 全选 POST /carts/selectAll

- 全不选 POST /carts/unSelectAll

# 35-添加地址

接口 POST /address

参数 consignee phone address